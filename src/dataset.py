# prepare dataset for usage
import json
import pandas as pd
import numpy as np

class Dataset:
    def __init__(self, json_path):
        self.json_path = json_path

        self._x = None
        self._y = None

        self.x_train = None
        self.y_train = None

        self.x_test = None
        self.y_test = None

        self.input_shape = None
        self.min = None
        self.ptp = None

        self.uid = None
        self.stations = None
        self.periods = None
        self.frc_dates_train = None
        self.frc_dates_test = None
        
        self.__idx = None
        self.__max_idx = None

        self._fixed_keys = ['flow_rate', 'rain', 'basin_rain', 'basin_maxtemp']
        self._fixed_window = 180

        self._eval()

    def _eval(self, ):
        with open(self.json_path, 'r') as f: setattr(self, 'json', json.load(f))
        self.stations = list(self.json['data'].keys())
        self.__max_idx = len(self.stations)
        self.periods = pd.to_datetime(self.json['periods']) # ?
        pass

    def _prepare_station_dataset(self, ):
        _data_raw = np.array([self.json['data'][self.uid][key]
                              for key in self._fixed_keys])
        
        _data_raw_min = _data_raw.min(1, keepdims=True)
        _data_raw_ptp = _data_raw.ptp(1, keepdims=True)
        
        self.min = _data_raw_min[0, 0]
        self.ptp = _data_raw_ptp[0, 0]
        
        _data_raw = (_data_raw - _data_raw_min) / _data_raw_ptp

        frc_dates = [self.periods[w + 120] for w in range(_data_raw.shape[1] - self._fixed_window)]
        _data_raw = [_data_raw[:, slice(w, w + self._fixed_window)]
                        for w in range(_data_raw.shape[1]\
                                       - self._fixed_window)]
        
        self._x = []
        self._y = []
        for _data in _data_raw:
            self._x.append(np.r_[_data[:, :60], _data[:, 60:120], _data[1:, 120:]])
            self._y.append(_data[0, 120:])

        self._x = np.asanyarray(self._x)
        self._y = np.asanyarray(self._y)

        self._x = self._x.reshape(self._x.shape[0], -1)
        self.input_shape = self._x.shape[1:]

        self.x_train = self._x[:-60]
        self.y_train = self._y[:-60]
        self.frc_dates_train = frc_dates[:-60]

        self.x_test = self._x[-60:]
        self.y_test = self._y[-60:]
        self.frc_dates_test = frc_dates[-60:]

        pass


    def iterstations(self, ):
        # prepare normatization ?
        return iter(self)


    def __iter__(self, ):
        self.__idx = 0
        return self


    def __next__(self, ):
        if self.__idx < self.__max_idx:
            self.uid = self.stations[self.__idx]
            self._prepare_station_dataset()
            # prepare any other needed data
            self.__idx += 1
            return self
        else:
            raise StopIteration
