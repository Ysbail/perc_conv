# define perc and conv models structs

from tensorflow.keras.models import Model
from tensorflow.keras.layers import (
    Input,
    Dense,
    Conv1D,
    Lambda,
    Reshape,
    Dropout,
    Flatten,
    Permute,
    concatenate,
    MaxPooling1D,
)

def gen_perc(input_shape=(None, )):
    _in = Input(input_shape)
    d1 = Dense(1024, activation='tanh')(_in)
    d2 = Dense(1024, activation='sigmoid')(d1)
    d3 = Dense(1024, activation='tanh')(d2)
    out = Dense(60, activation='sigmoid')(d3)
    return Model(inputs=_in, outputs=out)


def gen_conv(input_shape=(None, ), n_obs=8, n_frc=3):
    _in = Input(input_shape)
    re = Reshape((n_obs + n_frc, 60))(_in)
    in_obs = re[:, :n_obs, :]
    in_frc = re[:, n_obs:, :]
    re_obs = Permute((2, 1))(in_obs)
    re_frc = Permute((2, 1))(in_frc)
    
    conv_obs_1 = Conv1D(filters=16, kernel_size=1, strides=1,
                        activation='relu')(re_obs)
    conv_obs_3 = Conv1D(filters=16, kernel_size=3, strides=1,
                        activation='relu')(re_obs)
    conv_obs_5 = Conv1D(filters=16, kernel_size=5, strides=1,
                        activation='relu')(re_obs)
    
    conv_frc_1 = Conv1D(filters=16, kernel_size=1, strides=1,
                        activation='relu')(re_frc)
    conv_frc_3 = Conv1D(filters=16, kernel_size=3, strides=1,
                        activation='relu')(re_frc)
    conv_frc_5 = Conv1D(filters=16, kernel_size=5, strides=1,
                        activation='relu')(re_frc)
    
    concat_1 = concatenate([conv_obs_1, conv_frc_1])
    concat_3 = concatenate([conv_obs_3, conv_frc_3])
    concat_5 = concatenate([conv_obs_5, conv_frc_5])
    
    conv_concat_1 = Conv1D(filters=16, kernel_size=1, strides=1,
                           activation='relu')(concat_1)
    conv_concat_3 = Conv1D(filters=16, kernel_size=1, strides=1,
                           activation='relu')(concat_3)
    conv_concat_5 = Conv1D(filters=16, kernel_size=1, strides=1,
                           activation='relu')(concat_5)

    mp_1 = MaxPooling1D(2)(conv_frc_1)
    mp_3 = MaxPooling1D(2)(conv_frc_3)
    mp_5 = MaxPooling1D(2)(conv_frc_5)

    f_0 = Flatten()(conv_concat_1)
    f_1 = Flatten()(conv_concat_3)
    f_2 = Flatten()(conv_concat_5)
    f_3 = Flatten()(mp_1)
    f_4 = Flatten()(mp_3)
    f_5 = Flatten()(mp_5)

    concat_f = concatenate([f_0, f_1, f_2, f_3, f_4, f_5])
    drop = Dropout(0.5)(concat_f)
    out = Dense(60, activation='sigmoid')(drop)

    return Model(inputs=_in, outputs=out)

    
