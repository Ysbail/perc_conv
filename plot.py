# load dataset
# foreach model type
    # foreach station
        # load model and compile
        # train model and save
import os
import numpy as np
import tensorflow.keras.backend as K
from src.dataset import Dataset
from tensorflow.keras.models import load_model

import matplotlib.pyplot as plt

CWD = os.getcwd()
path = os.path.join(CWD , 'data/krk_obs.json')
datasets = Dataset(path)

for dataset in datasets.iterstations():
    for model_type in ['perc', 'conv']:
        K.clear_session()
        model = load_model(os.path.join(CWD, 'models/%s/%s_%03d.h5'%(model_type, model_type, int(dataset.uid))))
        yhat = model.predict(dataset.x_test)

        y = yhat * dataset.ptp + dataset.min
        l = dataset.y_test * dataset.ptp + dataset.min

        _mse = np.mean((y - l)**2)
        _mae = np.mean(abs(y - l))
        
        plt.plot(y[-1], 'r', label='frc')
        plt.plot(l[-1], 'b', label='obs')
        plt.legend()
        plt.title('%s | %s | mse: %.3f | mae : %.3f'%(dataset.frc_dates_test[-1].strftime('%Y-%m-%d'), model_type, _mse, _mae))
        # plt.show()
        plt.savefig(os.path.join(CWD, 'plots/%s_%03d.png'%(model_type, int(dataset.uid))))
        plt.close()
