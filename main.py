# load dataset
# foreach model type
    # foreach station
        # load model and compile
        # train model and save
import os
import tensorflow.keras.backend as K
from src.dataset import Dataset
from src.models import gen_perc, gen_conv
from tensorflow.keras.callbacks import (
    ModelCheckpoint,
    EarlyStopping
)

CWD = os.getcwd()
path = os.path.join(CWD , 'data/krk_obs.json')
datasets = Dataset(path)

best_model = os.path.join(CWD, 'models/temp.h5')
checkpoint = ModelCheckpoint(best_model,
                             monitor='val_mse',
                             save_best_only=True,
                             mode='auto')
earlystop = EarlyStopping(monitor='val_mse',
                          patience=50,
                          restore_best_weights=True,
                          mode='auto')

modes = {'perc': gen_perc, 'conv': gen_conv}

for dataset in datasets.iterstations():
    for model_type in ['perc', 'conv']:
        K.clear_session()
        print('### %s ### station %03d ###'%(model_type, int(dataset.uid)))
        model = modes[model_type](dataset.input_shape)
        model.compile(loss='logcosh', optimizer='adam', metrics=['mse'])
        model.fit(dataset.x_train,
                  dataset.y_train,
                  validation_split=0.2,
                  verbose=0,
                  epochs=1000,
                  callbacks=[checkpoint,
                             earlystop])
        print(model.evaluate(dataset.x_test, dataset.y_test))
        print('### %s ### station %03d ###'%(model_type, int(dataset.uid)))
        model.save(os.path.join(CWD, 'models/%s/%s_%03d.h5'%(model_type,
                                                             model_type,
                                                             int(dataset.uid))))
        
    
    
